# Eksamen i INF101 våren 2023: løsningsforslag

* Start-repo uten løsningsforslag: https://git.app.uib.no/ii/inf101/inf101exams/v-23-exam-files
* Sensorveiledning og løsningsforslag for oppgave 1-3: https://inf101v23.stromme.me/eksamen/sensorveiledning/

I dette repositoriet finner du kode-delen av et løsningsforslag for oppgave 2, samt løsningsforslag for oppgave 3 og 4.


# Opprinnelig tekst i README.md:

Oppgavene er beskrevet i Inspera (vurdering.uib.no) på både bokmål og nynorsk. For å gjøre det litt enklere for deg, har vi også lagt inn kopier av oppgavene her, slik at du slipper å navigere for mye frem og tilbake. Merk at oppgave 1 og 2 skal besvares *i Inspera*, mens oppgave 3 og 4 skal gjøres ved at du endrer på denne kodebasen -- du skal deretter lage en zip -fil som lastes opp til Inspera.

Skal besvares på Inspera:
* [Oppgave 1](./docs/04-OPG1-FLERVALG.md): flervalg
* [Oppgave 2](./docs/05-OPG2-TETROMINOFABRIKKEN.md): forklar hvordan skrive tester for Tetromino-fabrikken

Skal besvares ved å endre dette repoet, som deretter zippes og lastes opp til Inspera:
* [Oppgave 3](./docs/06-OPG3-WHACK-A-MOLE.md): legg til ny funksjonalitet i Whack-a-Mole
* [Oppgave 4](./docs/07-OPG4-PAINT.md): paint