package no.uib.inf101.exam23v.colorwhack;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.colorwhack.controller.ColorWhackController;
import no.uib.inf101.exam23v.colorwhack.model.ColorWhackModel;
import no.uib.inf101.exam23v.colorwhack.view.ColorWhackMainView;

import javax.swing.JFrame;
import java.util.Random;

/** Main class for the ColorWhack game. */
public class MainColorWhack {
  /**
   * Main method for the ColorWhack game.
   * 
   * @param args ignored
   */
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    ColorWhackModel model = new ColorWhackModel(eventBus, new Random());
    ColorWhackMainView view = new ColorWhackMainView(eventBus, model);
    new ColorWhackController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101 ColorWhack");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
