package no.uib.inf101.exam23v.paint.view;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import no.uib.inf101.eventbus.Event;
import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.eventbus.EventHandler;

/**
 * The main view of the paint program. This view contains the color
 * picker, the canvas, and the buttons.
 * 
 * This class is responsible for creating the other views. It also
 * listens for ModelChangedEvents, and repaints everything when it
 * receives one.
 * 
 * This view will also post events to the event bus when the user
 * clicks any buttons in the view; however it will not post any
 * events when the user interacts directly with the canvas. The
 * canvas subpanel is available through the getCanvasView() method.
 */
public class ViewMain extends JPanel implements EventHandler {

  private final ViewCanvas paintingView;

  /**
   * Create a new main view.
   * 
   * @param model The model to display.
   * @param eventBus The event bus on which to listen for
   *                ModelChangedEvents, and on which to send
   *                events when the user interacts with the view.
   */
  public ViewMain(ViewablePaintModel model, EventBus eventBus) {
    eventBus.register(this);
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    ViewColorPicker colorPickerView = new ViewColorPicker(model, eventBus);
    this.add(colorPickerView);

    this.paintingView = new ViewCanvas(model);
    this.add(this.paintingView);
    this.add(new ViewButtons(eventBus));
  }

  /** Gets the subview displaying the actual canvas. */
  public ViewCanvas getCanvasView() {
    return this.paintingView;
  }

  @Override
  public void handle(Event event) {
    this.repaint();
  }

}
