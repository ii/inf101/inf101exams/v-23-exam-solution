package no.uib.inf101.exam23v.paint.view;

import java.awt.Color;

import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

/**
 * This interface defines the methods that a model for a paint app
 * must implement to be displayed by the view.
 */
public interface ViewablePaintModel {

  /** Get the current pen color. */
  Color getPenColor();

  /** Get the canvas. */
  Iterable<GridCell<Color>> getCanvas();

  /** Get the canvas dimension. */
  GridDimension getImageDimension();
}
