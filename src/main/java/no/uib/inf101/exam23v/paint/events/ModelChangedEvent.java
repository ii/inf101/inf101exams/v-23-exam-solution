package no.uib.inf101.exam23v.paint.events;

import no.uib.inf101.eventbus.Event;

/**
 * An event sent from the model to the view when the model has
 * changed.
 */
public record ModelChangedEvent() implements Event {}
