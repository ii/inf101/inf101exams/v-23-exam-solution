package no.uib.inf101.exam23v.paint.events;

import no.uib.inf101.eventbus.Event;

/**
 * An event sent from the view to the controller when the user clicks
 * the save button.
 */
public record SaveClickedEvent() implements Event {}
