package no.uib.inf101.exam23v.paint.controller;

import no.uib.inf101.eventbus.Event;
import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.paint.PaintModel;
import no.uib.inf101.exam23v.paint.events.ClearClickedEvent;
import no.uib.inf101.exam23v.paint.events.ColorClickedEvent;
import no.uib.inf101.exam23v.paint.events.SaveClickedEvent;
import no.uib.inf101.exam23v.paint.view.ViewCanvas;
import no.uib.inf101.grid.GridDimension;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * This class handles events from the buttons in the GUI, 
 * including button clicks on the color buttons, the
 * clear button and the save button.
 * 
 * An object of this class is created will react to
 * the following events posted to the event bus:
 * 
 * - ColorClickedEvent
 * - SaveClickedEvent
 * - ClearClickedEvent
 * 
 * The class will update the model accordingly.
 */
public class ControllerButtonEvents {

  private final PaintModel model;

  /**
   * Create a new controller for handling button events.
   * 
   * @param model The model to update when buttons are clicked.
   * @param eventBus The event bus to listen to for button events.
   */
  public ControllerButtonEvents(PaintModel model, EventBus eventBus) {
    this.model = model;
    eventBus.register(this::handleButtonEvents);
  }

  private void handleButtonEvents(Event event) {
    if (event instanceof ColorClickedEvent colorClickedEvent) {
      model.setPenColor(colorClickedEvent.color());
    } else if (event instanceof SaveClickedEvent) {
      save();
    } else if (event instanceof ClearClickedEvent) {
      model.clear();
    }
  }

  private void save() {
    // This method is written with the help of CoPilot, using the prompt:
    // "Open a save dialog box that allows us to write a file name
    // and then save the current painting to that file.""
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setCurrentDirectory(new File("."));
    if (fileChooser.showSaveDialog(JFrame.getFrames()[0]) == JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      saveImageToFile(file);
    }
  }

  private void saveImageToFile(File file) {
    // This method is written with the help of CoPilot, using the prompts
    // given in quotes below, but with some modifications to fit the
    // current context.

    // "If file name does not end with .png, add this file extension."
    if (!file.getName().endsWith(".png")) {
      file = new File(file.getAbsolutePath() + ".png");
    }

    // "Create a png file that looks like what is painted by the
    // ViewCanvas view, and save it to the file. The picture should
    // have the same dimensions as the canvas from the model."
    GridDimension gd = model.getCanvas();
    Dimension dim = new Dimension(gd.cols(), gd.rows());
    BufferedImage img = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g = img.createGraphics();
    ViewCanvas view = new ViewCanvas(model);
    view.setSize(dim);
    view.paintComponent(g);

    try {
      ImageIO.write(img, "png", file);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
