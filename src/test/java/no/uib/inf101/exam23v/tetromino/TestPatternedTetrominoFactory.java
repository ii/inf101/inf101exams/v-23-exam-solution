package no.uib.inf101.exam23v.tetromino;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TestPatternedTetrominoFactory {
  private static final int NUMBER_OF_TRAILS = 10;
  
  @Test
  public void testFactoryProducingOnlyT() {
    PatternedTetrominoFactory factory = new PatternedTetrominoFactory("T");
    Tetromino t = Tetromino.newTetromino('T');
    
    for (int i = 0; i < NUMBER_OF_TRAILS; i++) {
      Tetromino tetro = factory.getNext();
      assertEquals(t, tetro);
    }
  }
}
